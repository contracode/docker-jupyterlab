# Jupyter Nvidia-Docker Stack

This is a ready-to-run GPU-enabled Docker image containing Jupyter applications and interactive computing tools. This project is a composition of two existing Docker images, based off of Project Jupyter's [docker-stacks](https://github.com/jupyter/docker-stacks) project and Nvidia's [CUDA](https://gitlab.com/nvidia/cuda/) Docker images.

### Screenshot

![Screenshot](https://gitlab.com/contracode/docker-jupyterlab/raw/master/screenshot.png "Screenshot of the JupyterLab application running in a web browser.")

### Prerequisites

* [Docker](https://www.docker.com)
* [nvidia-docker](https://github.com/NVIDIA/nvidia-docker)

## Getting Started

The following command uses the `nvidia-docker` utility to create a Docker container named `docker_jupyterlab`. After this command has been run, JupyterLab will be available via a web browser at `http://localhost:8888`, with the login password, `hello`.

```
nvidia-docker run \
              --name docker_jupyterlab \
              --publish 8888:8888 \
              --env GRANT_SUDO=yes \
              --env NVIDIA_VISIBLE_DEVICES=all \
              --env NVIDIA_DRIVER_CAPABILITIES=all \
              --user root \
              --detach \
              --interactive \
              --tty \
              contracode/docker-jupyterlab:latest \
              start.sh jupyter lab \
                  --NotebookApp.password='sha1:bafb785600a5:a873bd91bb8ee6aa7b5a2348abe5a67d0af785de'
```

If you would like to set a different password, other than the default `hello` implemented by this command, you will need to have the Jupyter Notebook application installed locally. Otherwise, you can generate a salted SHA-1 hash using a method of your choosing.

With Jupyter Notebook installed, you can obtain a proper, salted SHA-1 hash using the Python REPL. For example, from a Bash prompt:

```
$ python
Python 3.6.5 (default, Mar 29 2018, 03:28:50)
[GCC 5.4.0 20160609] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from notebook.auth import passwd
>>> passwd()
Enter password:
Verify password:
'sha1:ffcc47f2b41a:610e9c57b3bf31c371e6c45775ac54bfa0c98271'
>>>
```

From here, you can copy the generated SHA-1 hash for your new,  super-secret password (this one corresponding to the word *world*), and replace the hash within the `NotebookApp.password` directive, above.

### Installing

The command in the ***Getting Started*** section will automatically download and run this image from the [Docker Hub](https://hub.docker.com/r/contracode/docker-jupyterlab). However, if you would like to pull the image without running it, the following command will do so:

```
docker pull contracode/docker-jupyterlab
```

Otherwise, you can clone this repository and build the image yourself using the following:

```
git clone https://gitlab.com/contracode/docker-jupyterlab.git
cd docker-jupyterLab
docker build . -t latest
```

### Configuration

Since this Docker image is based off of the `nvidia/cuda:9.0-cudnn7-runtime` [image](https://hub.docker.com/r/nvidia/cuda/), produced by Nvidia, default values are provided for the `NVIDIA_VISIBLE_DEVICES`, `NVIDIA_DRIVER_CAPABILITIES`, and other GPU-specific environment values. However, please review `nvidia-container-runtime` [documentation](https://github.com/nvidia/nvidia-container-runtime#environment-variables-oci-spec), if there is anything you would like to tweak with respect to your GPU.

To modify settings for the Jupyter Notebook or JupyterLab applications, themselves, please see the Jupyter Docker Stacks documentation, [here](https://jupyter-docker-stacks.readthedocs.io/en/latest/).

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/contracode/docker-jupyterlab/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting merge requests to us.

## Authors

* **Jared Contrascere** - *Initial work* - [contracode](https://contracode.com)

See also, the list of [contributors](https://gitlab.com/contracode/docker-jupyterlab/project_members) who participated in this project.

## License

This project is licensed under the GPLv3 License - see the [LICENSE](https://gitlab.com/contracode/docker-jupyterlab/blob/master/LICENSE) file for details

## Acknowledgments

A huge **THANK YOU** goes out to the [authors](https://gitlab.com/nvidia/cuda/project_members) of Nvidia's [CUDA images](https://hub.docker.com/r/nvidia/cuda/):

* Felix Abecassis [@flx42](https://gitlab.com/flx42) · nvidia
* Jonathan Calmels [@3XX0](https://gitlab.com/3XX0) · nvidia
* Michael O'Connor [@moconnor725](https://gitlab.com/moconnor725) · nvidia

As well as the [docker-stacks](https://github.com/jupyter/docker-stacks) project's 88+ [contributors](https://github.com/jupyter/docker-stacks/graphs/contributors). **Thank you** for your hard work in making data science accessible to new learners, novices, and professionals around the world!
